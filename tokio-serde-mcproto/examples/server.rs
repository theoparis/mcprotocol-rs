use anyhow::Result;
use bytes::BytesMut;
use mcproto::v1_7_10::{Handshake, NextState, StatusToClient, StatusToServer};
use serde_json::json;
use tokio::{
	io::AsyncReadExt,
	net::{TcpListener, TcpStream},
};
use tokio_serde_mcproto::codec::MCProtoCodec;
use tokio_stream::{wrappers::TcpListenerStream, StreamExt};
use tokio_util::codec::{Decoder, Encoder};
use tracing::{debug, error, info};

async fn handle_login(mut socket: TcpStream) -> Result<()> {
	Ok(())
}

async fn handle_ping(mut socket: TcpStream) -> Result<()> {
	let mut codec = MCProtoCodec::<StatusToClient, StatusToClient>::default();

	let status_json = json!({
	   "description": {
			"text": "A minecraft server in Rust"
		},
		"players": {
			"max": 10,
			"online": 100000
		},
		"version": {
			"name": "1.7.10",
			"protocol": 5
		}
	});
	let status_packet = StatusToClient::StatusResponse {
		response: serde_json::to_string(&status_json)?,
	};

	let mut bytes = BytesMut::new();

	codec.encode(status_packet, &mut bytes)?;

	socket.try_write(&bytes)?;

	let mut codec = MCProtoCodec::<StatusToClient, StatusToServer>::default();

	let mut buffer = [0u8; 1024];
	let bytes_read = socket.read(&mut buffer).await?;

	if bytes_read == 0 {
		error!("failed to read bytes from socket");
	}
	let client_status_packet =
		codec.decode(&mut buffer.as_slice().into())?.unwrap();

	if let StatusToServer::Ping { time } = client_status_packet {
		debug!("{:#?}", client_status_packet);

		let status_packet = StatusToClient::Pong { time };

		let mut bytes = BytesMut::new();

		codec.encode(status_packet, &mut bytes)?;

		socket.try_write(&bytes)?;
	}

	Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
	tracing_subscriber::fmt::init();

	let listener_v4 = TcpListener::bind("0.0.0.0:22565").await.unwrap();
	let listener_v6 = TcpListener::bind("[::]:25565").await.unwrap();

	let mut incoming_connections = TcpListenerStream::new(listener_v4)
		.merge(TcpListenerStream::new(listener_v6));

	info!("Listening on :25565");

	while let Some(socket_result) = incoming_connections.next().await {
		let mut socket = socket_result?;

		let mut buffer = [0u8; 1024];
		let bytes_read = socket.read(&mut buffer).await?;

		if bytes_read == 0 {
			error!("failed to read bytes from socket");
		}

		let mut codec = MCProtoCodec::<Handshake, Handshake>::default();
		let handshake = codec.decode(&mut buffer.as_slice().into())?;

		if let Some(handshake) = handshake {
			match handshake.clone() {
				Handshake::Handshake { next_state, .. } => {
					debug!(
						"Received handshake packet with parameters: {:#?}",
						handshake
					);
					match next_state {
						NextState::Login => handle_login(socket).await?,
						NextState::Status => handle_ping(socket).await?,
						_ => {}
					}
				}
			}
		}
	}

	Ok(())
}
