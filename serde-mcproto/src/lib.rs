pub mod de;
pub mod error;
pub mod ser;
pub mod types;

pub use de::read::read_varint;
pub use ser::write::write_varint;
