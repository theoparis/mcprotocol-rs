use super::nbt::GZIPNBT;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Slot {
	id: i16,
	count: u8,
	damage: i16,
	tag: Option<GZIPNBT>,
}
