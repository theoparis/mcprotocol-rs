use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct NBT(nbt::Blob);

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct GZIPNBT(nbt::Blob);
