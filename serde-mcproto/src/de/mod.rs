use serde::{Deserialize, Deserializer};

pub use deserializer::MCProtoDeserializer;

mod deserializer;
pub(crate) mod read;

pub fn deserialize<'de, T, D>(deserializer: D) -> Result<T, D::Error>
where
	T: ?Sized + Deserialize<'de>,
	D: Deserializer<'de>,
{
	Deserialize::deserialize(deserializer)
}
